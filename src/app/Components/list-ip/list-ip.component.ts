  import { Component, OnInit } from '@angular/core';
  import { WhiteList } from 'src/app/Classes/WhiteList';
  import { WhiteListService } from 'src/app/Services/white-list.service';
  import { Router } from '@angular/router';
@Component({
  selector: 'app-list-ip',
  templateUrl: './list-ip.component.html',
  styleUrls: ['./list-ip.component.css']
})
export class ListIpComponent implements OnInit {
 
    clonedIps: WhiteList= new WhiteList("","");
    IP_REGEX = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
    load: boolean;
    deleting: boolean = false
    constructor(public WLService: WhiteListService,public location: Router) { }
  
    ngOnInit() {
  
      this.WLService.GetIpList().subscribe((data: WhiteList[]) => {
        console.log(data);
        this.WLService.IpList = data;
        this.load = true
      })
  
    }
  
    onRowEditInit(w:WhiteList) {
      this.clonedIps = { ...w };
    }
    onRowEditSave(ip: WhiteList) {
      if (ip.userIp) {
        delete this.clonedIps;
        this.WLService.UpdateIp(ip).subscribe(
          data => console.log("EDIT SUCCESS"),
          err => console.log("edit error")
        )
      }
    }
  
    Delete(ip: WhiteList) {
      this.deleting = false;
      this.WLService.DeleteIp(ip).subscribe(data => { 
        this.WLService.IpList.splice(this.WLService.IpList.indexOf(ip), 1) },
        err=>console.log(err))
    }
    onRowEditCancel(ip: WhiteList, index: number) {
      this.WLService.IpList[index] = this.clonedIps;
      delete this.clonedIps;
  
    }
    add() {
      
      this.location.navigate(['/AddPremission/'])
    }
  }
  
