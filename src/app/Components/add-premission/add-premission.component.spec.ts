import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPremissionComponent } from './add-premission.component';

describe('AddPremissionComponent', () => {
  let component: AddPremissionComponent;
  let fixture: ComponentFixture<AddPremissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPremissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPremissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
