import { Component, OnInit } from '@angular/core';
import { WhiteList } from 'src/app/Classes/WhiteList';
import { WhiteListService } from 'src/app/Services/white-list.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-premission',
  templateUrl: './add-premission.component.html',
  styleUrls: ['./add-premission.component.css']
})
export class AddPremissionComponent implements OnInit {

  constructor(public whiteListService: WhiteListService,public location:Location) { }
  newIpDetails:WhiteList
  premission:FormGroup
  get f() { return this.premission.controls; }
REGEX="^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
  ngOnInit() {
    document.getElementById('id01').style.display='block';
    this.premission = new FormGroup({
      userIp: new FormControl('', [Validators.required, Validators.pattern(this.REGEX)]),
      userDescription:new FormControl('')
    })
  }
  addIpPremission() {
    this.newIpDetails=new WhiteList(this.f.userIp.value,this.f.userDescription.value)
    this.whiteListService.addIp(this.newIpDetails).subscribe(
      data =>{this.whiteListService.IpList.push(this.newIpDetails)},
      err=>console.log("err",err),
      ()=>this.location.back()
    )
  }
}
